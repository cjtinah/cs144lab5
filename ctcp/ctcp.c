/******************************************************************************
 * ctcp.c
 * ------
 * Implementation of cTCP done here. This is the only file you need to change.
 * Look at the following files for references and useful functions:
 *   - ctcp.h: Headers for this file.
 *   - ctcp_iinked_list.h: Linked list functions for managing a linked list.
 *   - ctcp_sys.h: Connection-related structs and functions, cTCP segment
 *                 definition.
 *   - ctcp_utils.h: Checksum computation, getting the current time.
 *
 *****************************************************************************/

#include "ctcp.h"
#include "ctcp_linked_list.h"
#include "ctcp_sys.h"
#include "ctcp_utils.h"

#define FIN_flag 0
#define ACK_flag 1
#define RETRANSMISSION_MAX 5

/**
 * Connection state.
 *
 * Stores per-connection information such as the current sequence number,
 * unacknowledged packets, etc.
 *
 * You should add to this to store other fields you might need.
 */
struct ctcp_state {
  struct ctcp_state *next;  /* Next in linked list */
  struct ctcp_state **prev; /* Prev in linked list */

  conn_t *conn;             /* Connection object -- needed in order to figure
                               out destination when sending */
  linked_list_t *unack_segs;
  linked_list_t *output_segs;
  uint32_t seqno;
  uint32_t ackno;
  bool sentFIN;
  bool receivedFIN;
  ctcp_config_t *cfg;
  /* FIXME: Add other needed fields. */
};

struct ctcp_super_packet {
  uint16_t num_retransmissions;
  long sendTime;
  ctcp_segment_t *segment;
};

/**
 * Linked list of connection states. Go through this in ctcp_timer() to
 * resubmit segments and tear down connections.
 */
static ctcp_state_t *state_list;

/* FIXME: Feel free to add as many helper functions as needed. Don't repeat
          code! Helper functions make the code clearer and cleaner. */


ctcp_state_t *ctcp_init(conn_t *conn, ctcp_config_t *cfg) {
  /* Connection could not be established. */
  if (conn == NULL) {
    return NULL;
  }

  /* Established a connection. Create a new state and update the linked list
     of connection states. */
  ctcp_state_t *state = calloc(sizeof(ctcp_state_t), 1);
  state->next = state_list;
  state->prev = &state_list;
  if (state_list)
    state_list->prev = &state->next;
  state_list = state;

  /* Set fields. */
  state->conn = conn;
  state->seqno = 1;
  state->ackno = 1;
  state->sentFIN = false;
  state->receivedFIN = false;
  state->unack_segs = ll_create();
  state->output_segs = ll_create();
  state->cfg = cfg;
  /* FIXME: Do any other initialization here. */

  return state;
}

void ctcp_destroy(ctcp_state_t *state) {
  /* Update linked list. */
  if (state->next)
    state->next->prev = state->prev;

  *state->prev = state->next;
  conn_remove(state->conn);
  free(state->cfg);
  /* FIXME: Do any other cleanup here. */
  ll_destroy(state->unack_segs);
  ll_destroy(state->output_segs);

  free(state);
  end_client();
}

void ctcp_read(ctcp_state_t *state) {
  /* FIXME */
  if (ll_length(state->unack_segs)*MAX_SEGMENT_SIZE < state->cfg->other_window) {
    char buf[MAX_SEGMENT_SIZE] = {0};
    int dataLen = conn_input(state->conn, buf, MAX_SEGMENT_SIZE);
    if (dataLen != 0 && !state->sentFIN) {
      if (dataLen == -1) { // send FIN
        ctcp_segment_t *new_seg = create_new_segment(buf, state, dataLen, FIN_flag);
        new_seg->cksum = cksum(new_seg, sizeof(ctcp_segment_t));
        if (conn_send(state->conn, new_seg, ntohs(new_seg->len))<0) {
          ctcp_destroy(state);
        }
        state->seqno += 1;
        state->sentFIN = true;
        ctcp_super_packet_t *unack_seg = calloc(sizeof(ctcp_super_packet_t),1);
        unack_seg->num_retransmissions = 0;
        unack_seg->sendTime = current_time();
        unack_seg->segment = new_seg;
        ll_add(state->unack_segs, unack_seg);
      }
      else if (dataLen > 0) {
        ctcp_segment_t *new_seg = create_new_segment(buf, state, dataLen, -1);
        new_seg->cksum = cksum(new_seg, sizeof(ctcp_segment_t)+dataLen);
        if (conn_send(state->conn, new_seg, ntohs(new_seg->len))<0) {
          ctcp_destroy(state);
        }
        state->seqno += ntohs(new_seg->len)-sizeof(ctcp_segment_t);
        ctcp_super_packet_t *unack_seg = calloc(sizeof(ctcp_super_packet_t)+dataLen,1);
        unack_seg->num_retransmissions = 0;
        unack_seg->sendTime = current_time();
        unack_seg->segment = new_seg;
        ll_add(state->unack_segs, unack_seg);
      }
    }
  }
}

ctcp_segment_t *create_new_segment(char *buf, ctcp_state_t *state, int dataLen, int flags) {
  int length = 0;
  if (dataLen > 0) 
    length = dataLen;
  ctcp_segment_t *new_seg = calloc(sizeof(ctcp_segment_t)+length, 1);
  new_seg->cksum = 0;
  new_seg->seqno = htonl(state->seqno);
  new_seg->ackno = htonl(state->ackno);
  new_seg->window = htons(state->cfg->window);
  if (dataLen == -1) {
    new_seg->len = htons(sizeof(ctcp_segment_t));
    if (flags == FIN_flag) {
      new_seg->flags |= htonl(FIN);
    }
    if (flags == ACK_flag) {
      new_seg->flags |= htonl(ACK);
    }
  }
  else {
    memcpy(new_seg->data, buf, length);
    new_seg->len = htons(sizeof(ctcp_segment_t)+length);
  }
  return new_seg;
}

void ctcp_receive(ctcp_state_t *state, ctcp_segment_t *segment, size_t len) {
  /* FIXME */
  if (len >= ntohs(segment->len)) {
    uint16_t checksum = segment->cksum;
    segment->cksum = 0;
    if (checksum == cksum(segment, ntohs(segment->len))) {
      if (ntohs(segment->len) > 20) {  // data packet
        send_data_ACK(state, segment);
      }
      if (ntohl(segment->flags) & ACK) { // ACK packet 
        ll_node_t *curr_node = ll_front(state->unack_segs);
        int responseToFIN = 0;
        while(curr_node != NULL) {
          ctcp_super_packet_t *ack_seg = curr_node->object;
          ctcp_segment_t *seg = ack_seg->segment;
          if (ntohl(segment->ackno) > ntohl(seg->seqno)) {
            responseToFIN = ntohl(seg->flags)&FIN;
            ll_node_t *next_node = curr_node->next;
            free(seg);
            free(ack_seg);
            ll_remove(state->unack_segs, curr_node);
            curr_node = next_node;
          } 
          else {
            curr_node = curr_node->next;
          }
        }
        if (responseToFIN && state->receivedFIN) {
          ctcp_destroy(state);
        }
      }
      if (ntohl(segment->flags) & FIN) {
        receive_FIN(state, segment); 
      }
      else {
        free(segment);
      }
    }
  }
}

void send_data_ACK(ctcp_state_t *state, ctcp_segment_t *segment) {
  send_output(state, segment);
  if (ll_length(state->output_segs) <= state->cfg->window) {
    ctcp_segment_t *ack_seg = create_new_segment(NULL, state, -1, ACK_flag);
    ack_seg->ackno = htonl(state->ackno);
    ack_seg->cksum = cksum(ack_seg, sizeof(ctcp_segment_t));
    if (conn_send(state->conn, ack_seg, ntohs(ack_seg->len)) < 0) {
      ctcp_destroy(state);
    }
    free(ack_seg);
  }
}

void send_output(ctcp_state_t *state, ctcp_segment_t *segment) {
  if (ntohl(segment->seqno) == state->ackno) { // duplicate packet if <
    ll_node_t *curr_node = ll_front(state->output_segs);
    bool retransmitted_packet = false;
    while (curr_node!= NULL) {
      ctcp_segment_t *seg = curr_node->object;
      if (segment->seqno == seg->seqno) {
        retransmitted_packet = true;
      }
      curr_node = curr_node->next;
    }
    if (retransmitted_packet == false) {
      ll_add(state->output_segs, segment);
    }
    ctcp_output(state);
  }
}

void receive_FIN(ctcp_state_t *state, ctcp_segment_t *segment) {
  send_output(state, segment);
  ctcp_segment_t *ack_seg = create_new_segment(NULL, state, -1, ACK_flag);
  ack_seg->ackno = htonl(ntohl(segment->seqno) + 1);
  state->ackno = ntohl(ack_seg->ackno);
  ack_seg->cksum = cksum(ack_seg, sizeof(ctcp_segment_t));
  if (conn_send(state->conn, ack_seg, sizeof(ctcp_segment_t))<0) {
   ctcp_destroy(state);
  }
  free(ack_seg);
  free(segment);
  state->receivedFIN = true; 
  if (state->sentFIN && state->receivedFIN) {
    ctcp_destroy(state);
  }
}

void ctcp_output(ctcp_state_t *state) {
  /* FIXME */
  while (ll_length(state->output_segs) > 0) {
    ll_node_t *curr_output_node = state->output_segs->head;
    uint16_t prev_ackno = state->ackno;
    while (curr_output_node != NULL) {
      size_t space = conn_bufspace(state->conn);
      ctcp_segment_t *segment = curr_output_node->object;
      uint16_t seg_len = ntohs(segment->len);
      if (space >= seg_len) {
        if (state->ackno == htonl(segment->seqno)) {
          conn_output(state->conn, segment->data, seg_len-sizeof(ctcp_segment_t));
          ll_node_t *next_node = curr_output_node->next;
          ll_remove(state->output_segs, curr_output_node);
          curr_output_node = next_node;
          state->ackno += seg_len-sizeof(ctcp_segment_t);
        }
      }
      else curr_output_node = curr_output_node->next;
    } if (prev_ackno == state->ackno) break;
    prev_ackno = state->ackno;
  }
}

void ctcp_timer() {
  /* FIXME */
  ctcp_state_t *curr_state = state_list;
  while (curr_state != NULL) {
    ll_node_t *curr_unack_node = ll_front(curr_state->unack_segs);
    while (curr_unack_node != NULL) {
      ctcp_super_packet_t *super_seg = curr_unack_node->object;
      int time_diff = current_time() - super_seg->sendTime;
      if (time_diff > curr_state->cfg->rt_timeout) {
        if (super_seg->num_retransmissions >= RETRANSMISSION_MAX) {
          ctcp_destroy(curr_state);
        }
        if (conn_send(curr_state->conn, super_seg->segment, ntohs(super_seg->segment->len))<0) {
          ctcp_destroy(curr_state);
        }
        super_seg->sendTime = current_time();
        super_seg->num_retransmissions++;
      }
      curr_unack_node = curr_unack_node->next;
    }
    curr_state = curr_state->next;
  }
}
